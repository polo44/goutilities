package goutilities

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"
)

// ErrorMode handles error mode
var ErrorMode bool = false

// DebugMode handles debug mode
var DebugMode bool = false

// APIKEY handles api key
var APIKEY string = ""

// Init inits the utilities variable package
func Init(errMode bool, dbgMode bool, apiKey string) {
	ErrorMode = errMode
	DebugMode = dbgMode
	APIKEY = apiKey
}

// ErrorsRender returns an error as a string based on the ProjectSettings ErrorMode
func ErrorsRender(msg string, err error) string {

	if ErrorMode && err != nil {
		return msg + " | " + err.Error()
	}

	return msg
}

// MyHash handles hashing of something
func MyHash(toHash []byte) string {
	//sum := sha256.Sum256(*toHash)
	h := sha256.New()
	h.Write(toHash)
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}

// MyHashFile handles hashing of file
func MyHashFile(toHash []byte) string {
	//sum := sha256.Sum256(*toHash)
	h := md5.New()
	h.Write(toHash)
	return hex.EncodeToString(h.Sum(nil))
}

// APIAuth handles authentication for api endpoints
func APIAuth(r *http.Request) bool {

	if DebugMode {
		return true
	}

	// We check the api key
	apiKey := r.Header.Get("apikey")
	return apiKey == APIKEY
}

// APIResult write a result & the http code on a ResponseWriter
func APIResult(w *http.ResponseWriter, code int, message string) {

	http.ResponseWriter(*w).Header().Set("Content-Type", "application/json")
	http.ResponseWriter(*w).WriteHeader(code)

	dataStruct := struct {
		Result string `json:"result"`
	}{Result: message}
	data, err := json.Marshal(&dataStruct)
	if err != nil {
		fmt.Fprint(*w, `{"result": "can't marshal result."}`)
		return
	}

	fmt.Fprintf(*w, "%s", data)
}

// APIBody write a result & the http code on a ResponseWriter
func APIBody(w *http.ResponseWriter, message *[]byte) {

	http.ResponseWriter(*w).WriteHeader(200)
	fmt.Fprintf(*w, "%s", *message)
}

// APIBodyString write a result & the http code on a ResponseWriter
func APIBodyString(w *http.ResponseWriter, message string) {

	http.ResponseWriter(*w).WriteHeader(200)
	fmt.Fprintf(*w, "%s", message)
}
