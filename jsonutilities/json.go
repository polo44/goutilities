package jsonutilities

import (
	"encoding/json"
	"io"
	"io/ioutil"
)

// UnmarshalFile unmarshal a json file onto a variable
func UnmarshalFile(filePath string, dstVar interface{}) error {

	// We read the file
	b, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}

	// We unmarshal the json
	err = json.Unmarshal(b, dstVar)
	if err != nil {
		return err
	}

	return nil
}

// UnmarshalReader unmarshal an io.Reader onto a variable
func UnmarshalReader(r io.Reader, dstVar interface{}) error {

	// We read the file
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}

	// We unmarshal the json
	err = json.Unmarshal(b, dstVar)
	if err != nil {
		return err
	}

	return nil
}
